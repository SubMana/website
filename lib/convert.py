import os, glob
import md2html
excludeThese = ["../README.md"]
skip = "false"

for post in glob.iglob('../**/*.md'):
    for items in excludeThese:
        if post == items:
            skip = "true"
    if skip == "false":
        # Read in the template
        with open('../template.html', 'r') as file :
            htmlFile = file.read()
        # Read in the post
        with open(post, 'r') as file :
            postFile = file.read()
            postFile = md2html.markdown(postFile)
        

        # Shove the post into the HTML template somewhere in the body
        htmlFile = htmlFile.replace('PAGE_CONTENT', postFile)

        # Overwrite the post
        with open(post, 'w') as file:
            file.write(htmlFile)
        
        # Rename the post so it becomes a valid HTML document
        os.rename(post, post[:-3]+".html")
        print("Converted "+post+" into "+post[:-3]+".html")
    skip = "false"

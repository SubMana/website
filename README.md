# Website

My website. Includes build scripts, and will probably store the site's content and templates as well. Hosted externally.

# To do
- Add more content.
- Fix mobile formatting.
- Create a better template so I don't have to copy paste so much.

# Credits
Full credit to Armin Ronacher (https://lucumr.pocoo.org/) for the CSS, I simply copied this design and made some personal changes.

I used markdown2 to convert my posts into webpages: https://pypi.org/project/markdown2/
